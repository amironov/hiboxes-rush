// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "map.h"
#include <unistd.h>

void	ft_any_solution_map(int map[4][4])
{
	map[0][0] = 1;
	map[0][1] = 2;
	map[0][2] = 3;
	map[0][3] = 4;
	map[1][0] = 2;
	map[1][1] = 1;
	map[1][2] = 4;
	map[1][3] = 3;
	map[2][0] = 3;
	map[2][1] = 4;
	map[2][2] = 1;
	map[2][3] = 2;
	map[3][0] = 4;
	map[3][1] = 3;
	map[3][2] = 2;
	map[3][3] = 1;
}

void	ft_print_map(const int map[4][4])
{
	int row;
	int col;
	char symbol;

	row = 0;
	while (row < 4)
	{
		col = 0;
		while (col < 4)
		{
			symbol = '1' + map[row][col];
			write(1, &symbol, 1);
			write(1, &"   \n"[col], 1);
			col++;
		}
		row++;
	}
}
