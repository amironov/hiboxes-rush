// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "solve.h"
#include "guide.h"
#include "map.h"
#include "permutation.h"
#include <stdio.h>

int	ft_guide_from_solution(const int solution[4][4], int guide[16])
{
	ft_guide_col(solution, 0, &guide[0], &guide[4]);
	ft_guide_col(solution, 1, &guide[1], &guide[5]);
	ft_guide_col(solution, 2, &guide[2], &guide[6]);
	ft_guide_col(solution, 3, &guide[3], &guide[7]);
	ft_guide_row(solution, 0, &guide[8], &guide[12]);
	ft_guide_row(solution, 1, &guide[9], &guide[13]);
	ft_guide_row(solution, 2, &guide[10], &guide[14]);
	ft_guide_row(solution, 3, &guide[11], &guide[15]);
	return (1);
}

int	ft_make_solution(int solution[4][4], int index)
{
	int row;
	int col;
	int digits[4];
	int n_digits;

	row = 0;
	while (row < 4) {
		col = 0;
		while (col < 4) {
			ft_available_digits(solution, row, col, digits, &n_digits);
			if (n_digits > 0)
			{
				solution[row][col] = digits[index % n_digits];
				index /= n_digits;
			}
			else
			{
				return (0);
			}
			col++;
		}
		row++;
	}
	return (1);
}

int	ft_solve(const int expected_guide[16], int solution[4][4])
{
	int i;
	int guide[16];

	i = 0;
	while (i < 144 * 12)
	{
		if (ft_make_solution(solution, i)
			&& ft_guide_from_solution(solution, guide)
			&& ft_equal_guide(expected_guide, guide))
		{
			return (1);
		}
		i++;
	}
	return (0);
}
