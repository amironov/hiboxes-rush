// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "guide.h"

void	ft_guide_row(const int map[4][4], int row, int *a, int *b)
{
	int i;
	int value_a;
	int value_b;
	value_a = -1;
	value_b = -1;
	*a = 0;
	*b = 0;
	i = 0;
	while (i < 4)
	{
		if (map[row][i] > value_a) {
			value_a = map[row][i];
			(*a)++;
		}

		if (map[row][3 - i] > value_b) {
			value_b = map[row][3 - i];
			(*b)++;
		}
		i++;
	}
}

void	ft_guide_col(const int map[4][4], int col, int *a, int *b)
{
	int i;
	int value_a;
	int value_b;
	value_a = -1;
	value_b = -1;
	*a = 0;
	*b = 0;
	i = 0;
	while (i < 4)
	{
		if (map[i][col] > value_a) {
			value_a = map[i][col];
			(*a)++;
		}

		if (map[3 - i][col] > value_b) {
			value_b = map[3 - i][col];
			(*b)++;
		}
		i++;
	}
}

int	ft_equal_guide(const int expected_guide[16], const int guide[16])
{
	int i;
	i = 0;
	while (i < 16)
	{
		if (expected_guide[i] != guide[i])
		{
			return (0);
		}
		i++;
	}
	return (1);
}

int	ft_extract_guide(const char *arg, int digits[16])
{
	int i;
	char symbol;
	i = 0;
	while (i < (16 * 2 - 1))
	{
		symbol = arg[i];
		if (symbol == ' ' && (i % 2) == 1)
		{}
		else if ('1' <= symbol && symbol <= '4' && (i % 2) == 0)
		{
			digits[i / 2] = symbol - '0';
		}
		else
		{
			return (0);
		}
		i++;
	}
	return (arg[i] == 0);
}
