// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "permutation.h"

int	ft_get_digit(int available_digits[4], int index)
{
	int digit = 0;
	while (1) {
		if (available_digits[digit] == 0)
		{
			digit++;
		}
		else if (index > 0)
		{
			digit++;
			index--;
		}
		else
		{
			break;
		}
	}
	available_digits[digit] = 0;
	return digit;
}

void	ft_get_permutation(int index, int *permutation, int count)
{
	int available_digits[count];
	int i = 0;
	while (i < 4)
	{
		available_digits[i] = 1;
		i++;
	}
	
	i = 0;
	while (i < count)
	{
		permutation[i] = ft_get_digit(available_digits, index % (count - i));
		index /= (count - i);
	}
}

void	ft_discard_digits(const int solution[4][4],
						  int row,
						  int col,
						  int digits[4])
{
	int i;
	i = 0;
	while (i < row)
	{
		digits[solution[i][col]] = 0;
		i++;
	}
	
	i = 0;
	while (i < col)
	{
		digits[solution[row][i]] = 0;
		i++;
	}
}

void	ft_available_digits(const int solution[4][4],
							int row,
							int col,
							int digits[4],
							int *number_of_digits)
{
	int i;
	int j;
	i = 0;
	while (i < 4)
	{
		digits[i] = (1);
		i++;
	}
	
	ft_discard_digits(solution, row, col, digits);
	
	i = 0;
	j = 0;
	while (i < 4)
	{
		if (digits[i])
		{
			digits[j] = i;
			j++;
		}
		i++;
	}
	*number_of_digits = j;
}
