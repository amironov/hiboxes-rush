// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "solve.h"

int	ft_solve_inc_row(const struct s_context context, int row, int col,
					 struct s_solution *solution)
{
	struct s_context local_context;
	local_context = context;
	while (ft_inc_row_option(&local_context, row))
	{
		if (ft_build_solution(local_context, solution)
			|| ft_solve_inc(local_context, row + 1, col, solution)
			|| ft_solve_inc(local_context, row, col + 1, solution))
		{
			return (1);
		}
	}
	return (0);
}

int	ft_solve_inc_col(const struct s_context context, int row, int col,
					 struct s_solution *solution)
{
	struct s_context local_context;
	local_context = context;
	while (ft_inc_col_option(&local_context, col))
	{
		if (ft_build_solution(local_context, solution)
			|| ft_solve_inc(local_context, row + 1, col, solution)
			|| ft_solve_inc(local_context, row, col + 1, solution))
		{
			return (1);
		}
	}
	return (0);
}

int	ft_solve_inc(const struct s_context context, int row, int col,
				 struct s_solution *solution)
{
	if (row >= 4 || col >= 4)
	{
		return (0);
	}
	
	return (ft_solve_inc_row(context, row, col, solution)
			|| ft_solve_inc_col(context, row, col, solution)
			|| ft_solve_inc(context, row + 1, col, solution)
			|| ft_solve_inc(context, row, col + 1, solution));
}

int	ft_solve(struct s_problem problem, struct s_solution *solution)
{
	struct s_context context;
	return (ft_make_context(problem, &context)
			&& (ft_build_solution(context, solution)
				|| ft_solve_inc(context, 0, 0, solution)));
}
