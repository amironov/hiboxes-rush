// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "context.h"

int	ft_inc_option(const struct s_line lines[24], struct s_option *option)
{
	int i = option->line_index + 1;
	while (i < 24)
	{
		if (lines[i].guide.a == option->guide.a
			&& lines[i].guide.b == option->guide.b)
		{
			option->line_index = i;
			return (1);
		}
		i++;
	}
	return (0);
}

int	ft_inc_row_option(struct s_context *context, int row)
{
	return ft_inc_option(context->lines, &(context->option_for_row[row]));
}

int	ft_inc_col_option(struct s_context *context, int col)
{
	return ft_inc_option(context->lines, &(context->option_for_col[col]));
}

int	ft_make_context(const struct s_problem problem, struct s_context *context)
{
	int i;
	ft_make_lines(context->lines);
	i = 0;
	while (i < 4)
	{
		context->option_for_row[i].line_index = -1;
		context->option_for_row[i].guide = problem.row_guides[i];
		if (!ft_inc_row_option(context, i))
		{
			return (0);
		}
		
		context->option_for_col[i].line_index = -1;
		context->option_for_col[i].guide = problem.col_guides[i];
		if (!ft_inc_col_option(context, i))
		{
			return (0);
		}
		
		i++;
	}
	return (1);
}
