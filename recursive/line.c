// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "line.h"

int	ft_is_unique(const int d[4])
{
	return (1 << d[0] | 1 << d[1] | 1 << d[2] | 1 << d[3]) == 0b1111;
}

void	ft_set_guide_for_line(struct s_line *line)
{
	int prev_digit_a;
	int prev_digit_b;
	int i;

	prev_digit_a = -1;
	prev_digit_b = -1;
	line->guide.a = 0;
	line->guide.b = 0;
	i = 0;
	while (i < 4)
	{
		if (line->digits[i] > prev_digit_a)
		{
			prev_digit_a = line->digits[i];
			line->guide.a++;
		}
		
		if (line->digits[4 - i - 1] > prev_digit_b)
		{
			prev_digit_b = line->digits[4 - i - 1];
			line->guide.b++;
		}
		i++;
	}
}

void	ft_mem_cpy_int(int *dst, const int *src, int len)
{
	while (len > 0)
	{
		*dst = *src;
		dst++;
		src++;
		len--;
	}
}

void	ft_make_lines(struct s_line lines[24])
{
	int i;
	int j;
	int n;
	int digits[4];
	
	i = 0;
	while (i < 4 * 4 * 4 * 4)
	{
		n = i;
		i++;
		j = 0;
		while (j < 4)
		{
			digits[j] = n % 4;
			n /= 4;
			j++;
		}
		if (ft_is_unique(digits))
		{
			ft_mem_cpy_int(lines->digits, digits, 4);
			ft_set_guide_for_line(lines);
			lines++;
		}
	}
}
