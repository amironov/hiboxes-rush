// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "solution.h"
#include <unistd.h>

void	ft_print_solution(struct s_solution solution)
{
	int row;
	int col;
	char symbol;

	row = 0;
	while (row < 4)
	{
		col = 0;
		while (col < 4)
		{
			symbol = '1' + solution.map[row][col];
			write(1, &symbol, 1);
			write(1, &"   \n"[col], 1);
			col++;
		}
		row++;
	}
}

int	ft_build_cell(const struct s_context context, struct s_solution *solution,
				  int row, int col)
{
	int number_from_row;
	int row_option_index;
	int number_from_col;
	int col_option_index;

	row_option_index = context.option_for_row[row].line_index;
	number_from_row = context.lines[row_option_index].digits[col];
	col_option_index = context.option_for_col[col].line_index;
	number_from_col = context.lines[col_option_index].digits[row];
	if (number_from_row != number_from_col)
	{
		return (0);
	}
	solution->map[row][col] = number_from_row;
	return (1);
}

int	ft_build_solution(const struct s_context context,
					  struct s_solution *solution)
{
	int row;
	int col;
	row = 0;
	while (row < 4)
	{
		col = 0;
		while (col < 4)
		{
			if (!ft_build_cell(context, solution, row, col))
			{
				return (0);
			}
			col++;
		}
		row++;
	}
	return (1);
}
