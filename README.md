# Hiboxes Rush
Solutions for one of hard problems.
Done for fun. No 

## Naive solution
* requires only basic knowlege
* slow (can take up to 40s)

## Solution that checks possible permutations 
* requires only basic knowlege
* resembles naive solution
* medium complexity
* quick

## Recursive solution
* requires understating of
  * `struct`s
  * nested pointers
  * recursion
* complex
* quick

## All solutions
* 576 in total
* [link](./all_solutions.txt)
