// Copyright (c) 2020 Anton Mironov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "solve.h"
#include "guide.h"

int	ft_is_unique_row(int map[4][4], int row)
{
	return (1 << map[row][0]
			| 1 << map[row][1]
			| 1 << map[row][2]
			| 1 << map[row][3])
	== 0b1111;
}

int	ft_is_unique_col(int map[4][4], int col)
{
	return (1 << map[0][col]
			| 1 << map[1][col]
			| 1 << map[2][col]
			| 1 << map[3][col])
	== 0b1111;
}

int ft_increment_map(int map[4][4])
{
	int i;
	int *digits;
	digits = (int *)map;
	
	i = 0;
	digits[i]++;
	while (digits[i] == 4)
	{
		digits[i] = 0;
		i++;
		if (i == 16)
		{
			return 0;
		}
		digits[i]++;
	}
	return (1);
}

int ft_solve(int map[4][4], const int expected_guide[16])
{
	int guide[16];
	while (1)
	{
		if (ft_is_unique_row(map, 0) && ft_is_unique_row(map, 1)
			&& ft_is_unique_row(map, 2) && ft_is_unique_row(map, 3)
			&& ft_is_unique_col(map, 0) && ft_is_unique_col(map, 1)
			&& ft_is_unique_col(map, 2) && ft_is_unique_col(map, 3))
		{
			ft_guide_col(map, 0, &guide[0], &guide[4]);
			ft_guide_col(map, 1, &guide[1], &guide[5]);
			ft_guide_col(map, 2, &guide[2], &guide[6]);
			ft_guide_col(map, 3, &guide[3], &guide[7]);
			ft_guide_row(map, 0, &guide[8], &guide[12]);
			ft_guide_row(map, 1, &guide[9], &guide[13]);
			ft_guide_row(map, 2, &guide[10], &guide[14]);
			ft_guide_row(map, 3, &guide[11], &guide[15]);
			if (ft_equal_guide(expected_guide, guide))
			{
				return (1);
			}
		}
		ft_increment_map(map);
	}
	return (0);
}
